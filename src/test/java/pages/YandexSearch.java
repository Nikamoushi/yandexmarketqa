package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class YandexSearch {

    public YandexSearch search(String query) {
        $(By.xpath("//input[@aria-label=\"Запрос\"]")).setValue(query).pressEnter();
        return this;
    }

    public void clickLinkText(String text) {
        $(By.xpath("//div[text()='" + text + "']")).click();
       // $(By.xpath("//div[text()=' — выбор товаров и места их покупки']")).waitUntil(Condition.visible, 10).click();
    }
    public void clickButton (String xpath){
        $(By.xpath(xpath)).click();
    }

}




