package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class YandexMarket {

    public void searchYM(String query) {
        $(By.xpath("//input[@aria-labelledby=\"header-search header-search-label\"]")).setValue(query).pressEnter();
    }
    public void setMinPrice(String minPrice){
        $(By.xpath("//input[@id='glpricefrom']")).setValue(minPrice);

    }
    public void setMaxPrice(String maxPrice){
        $(By.xpath("//input[@id='glpriceto']")).setValue(maxPrice);
    }

    public void clickLinkText(String text) {

    }

    public void clickOnProduct(String xpath) {
        $(By.xpath(xpath)).click();

    }
}
