package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MarketProduct {

    public void checkText(String xpath, String text) {
        $(By.xpath(xpath)).shouldHave(Condition.text(text));
    }

    public void clickButton(String xpath) {
        $(By.xpath(xpath)).click();

    }
    public void scrollTo(String xpath) {
        $(By.xpath(xpath)).scrollTo();
    }
}
